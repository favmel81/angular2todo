<?php


namespace app\forms;

use app\models\User;


class ChangePassword extends User
{

    public $password1;


    public function rules()
    {
        return [

            [['password', 'password1'], 'trim'],
            ['password', 'required', 'message' => 'Введите пароль'],
            ['password1', 'required', 'message' => 'Введите повтор пароля'],
            ['password', 'compare', 'compareAttribute' => 'password1',
             'message'                                 => 'Пароли не совпадают']
        ];
    }

    public function savePassword() {
        parent::changePassword($this->password);
    }

} 