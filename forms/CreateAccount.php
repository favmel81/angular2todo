<?php

namespace app\forms;

use yii\helpers\ArrayHelper;
use app\models\User;

class CreateAccount extends User
{

    public $password1;
    public $confirm;



    public function rules()
    {

        return [

            [['login', 'email', 'firstname', 'middlename', 'lastname', 'address', 'password', 'password1'], 'trim'],
            ['login', 'required', 'message' => 'Поле Логин не может быть пустым!'],
            ['login', 'match', 'pattern' => '~^[a-z_]+[_0-9a-z]+$~i',
                'message'                   => 'Поле Логин содержит неверные символы!'],
            ['login', 'unique',
                'message' => 'Такой Логин уже используется'],
            ['email', 'required', 'message' => 'Поле Email не может быть пустым!'],
            ['email', 'email', 'message' => 'Поле Email содержит неверные символы!'],
            ['email', 'unique',
                'message' => 'Такой Email уже используется'],
            ['lastname', 'required', 'message' => 'Введите Фамилию'],
            ['firstname', 'required', 'message' => 'Введите Имя'],
            ['middlename', 'required', 'message' => 'Введите Отчество'],
           /* ['sex', 'required', 'message' => 'Укажите свой пол'],
            ['sex', 'in', 'range' => array_keys(self::getSexList()),
                'message'            => 'Укажите свой пол'],*/
           /* ['country', 'required', 'message' => 'Выберите страну'],
            ['country', 'in', 'range' => ArrayHelper::map(
                Countries::getList(), 'id', 'id'
            ), 'message'              => 'Выберите страну'],*/
            ['password', 'required', 'message' => 'Введите пароль'],
            ['password1', 'required', 'message' => 'Введите подтверждение пароля'],
            ['password', 'compare', 'compareAttribute' => 'password1',
                'message'                                 => 'Пароли не совпадают'],
            ['confirm', 'required', 'isEmpty' => function ($value) {
                return (int)$value <= 0;
            },
                'message'                        => 'Вы должны согласиться с условиями'],
            //['confirm', 'compare', 'compareValue' => 1, 'message' => 'Вы должны согласиться с условиями']

        ];


    }

}