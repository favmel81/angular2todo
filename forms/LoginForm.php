<?php


namespace app\forms;

use yii\base\Model;
use yii2utils\services\UserManager;


class LoginForm extends Model {

    const REMEMBER_DAYS = 30;
    public $login;
    public $password;
    public $remember;


    public function rules() {
        return [
          [['login', 'password'], 'trim'],
          ['login', 'required', 'message' => 'Введите логин'],
          ['password', 'required', 'message' => 'Введите пароль'],
          ['remember', 'boolean']
        ];
    }

    public function login() {
        if($this->hasErrors()) {
            throw new \Exception('Validation expected');
        }

        $userManager = new UserManager();
        $authResult = $userManager->authByLoginPassword(
            $this->login,
            $this->password,
            $this->remember ? self::REMEMBER_DAYS:false
        );

        if($authResult !== true) {
            $attributeName = array_keys($authResult)[0];
            $this->addError($attributeName, $this->getLiteralError($authResult[$attributeName]));
            return false;
        }
        return true;
    }

    protected function getLiteralError($error) {
        $errors = [
            UserManager::MESSAGE_INVALID_PASSWORD => 'Неверный пароль',
            UserManager::MESSAGE_USER_BLOCKED => 'Пользователь заблокирован',
            UserManager::MESSAGE_USER_IS_NOT_FOUND => 'Пользователь с указанным логином не найден'
        ];

        return $errors[$error];
    }


} 