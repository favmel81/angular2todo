<?php

namespace app\forms;

use yii\helpers\ArrayHelper;
use app\models\User;

class EditAccount extends User
{

    public $password1;



    public function rules()
    {

        return [

            [['login', 'email', 'firstname', 'middlename', 'lastname', 'address', 'password', 'password1'], 'trim'],
            ['login', 'required', 'message' => 'Поле Логин не может быть пустым!'],
            ['login', 'match', 'pattern' => '~^[a-z_]+[_0-9a-z]+$~i',
                'message'                   => 'Поле Логин содержит неверные символы!'],
            ['login', 'unique',
                'message' => 'Такой Логин уже используется',
            'filter' => ['!=', 'id', $this->id]
            ],
            ['email', 'required', 'message' => 'Поле Email не может быть пустым!'],
            ['email', 'email', 'message' => 'Поле Email содержит неверные символы!'],
            ['email', 'unique',
                'message' => 'Такой Email уже используется',
                'filter' => ['!=', 'id', $this->id]
            ],
            ['lastname', 'required', 'message' => 'Введите Фамилию'],
            ['firstname', 'required', 'message' => 'Введите Имя'],
            ['middlename', 'required', 'message' => 'Введите Отчество'],
           /* ['sex', 'required', 'message' => 'Укажите свой пол'],
            ['sex', 'in', 'range' => array_keys(self::getSexList()),
                'message'            => 'Укажите свой пол'],*/
           /* ['country', 'required', 'message' => 'Выберите страну'],
            ['country', 'in', 'range' => ArrayHelper::map(
                Countries::getList(), 'id', 'id'
            ), 'message'              => 'Выберите страну'],*/
            ['password', 'required', 'message' => 'Введите пароль'],
            ['password1', 'required', 'message' => 'Введите подтверждение пароля'],
            ['password', 'compare', 'compareAttribute' => 'password1',
                'message'                                 => 'Пароли не совпадают']

        ];


    }

}