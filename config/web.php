<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '6JahrlGIcPQI1DaV5dkf9xp2jt-fKqBI',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'user'         => [
            'identityClass'   => 'app\models\User',
            'loginUrl' => '/account/login',
            'enableAutoLogin' => true/*,
            'identityCookie' => [
                'name' => 'authCookie',
                'domain' => '.domain',
                'path' => '/',
            ]*/
        ],

        'errorHandler' => [
            'errorAction' => 'index/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'assetManager' => [
            'bundles' => false
        ],
        'urlManager' => require(__DIR__ . '/routes.php')
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

if (YII_ENV_PROD) {
    $config['components']['cache'] = [
        'class' => 'yii\caching\FileCache',
    ];
}

return $config;
