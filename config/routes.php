<?php

return [
    'enablePrettyUrl'     => true,
    'showScriptName'      => false,
    'enableStrictParsing' => true,
    //'suffix' => '/',
    'rules'               => [
        '/' => 'index/index',
        [
            'pattern'  => 'api/<controller:[a-zA-Z0-9_-]+>/<action:[a-zA-Z0-9_-]+>',
            'route'    => 'api/<controller>/<action>',
            'defaults' => [
                'action' => 'index'
            ]
        ],
        '<controller:(categories|account)>' => 'index/index',
    ],
];