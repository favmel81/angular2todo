<?php

namespace app\controllers\api;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii2utils\traits\CurrentUserIdTrait;
use app\models\Category;

class CategoriesController extends Controller
{
    use CurrentUserIdTrait;

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ]
            ],
            [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'move',
                            'delete',
                            'save',
                            'index',

                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
                'denyCallback' => function ($rule, $action) {

                    switch($action->id) {

                        case 'move':
                        case 'delete':
                        case 'save':
                            $data = [
                                'success' => false,
                                'auth' => true
                            ];
                            break;
                        default:
                            $data = [
                                'success' => false,
                                'auth' => false
                            ];

                    }

                    Yii::$app->getResponse()->data = $data;
                }
            ]
        ];

    }

    public function actionIndex()
    {
        return [
            'success' => true,
            'data' => Category::find()
                ->where(['user_id' => $this->getUserId()])
                ->orderBy(['ordered' => SORT_ASC])
                ->asArray()
                ->all()
        ];
    }

    public function actionSave()
    {
        $request = Yii::$app->request;
        $userId = $this->getUserId();

        if ($request->isPost) {
            $data = Yii::$app->request->post();
            $id =  isset($data['id'])?(int)$data['id']: 0;

            if ($id) {
                $category = Category::findOne($id);
                if(!$category) {
                    return [
                        'success' => false,
                        'alert' => [
                            'message' => 'Нет такой категории',
                            'redirect' => '/',
                            'timeout' => 10
                        ]
                    ];
                }

                if($category->user_id != $userId) {
                    return [
                        'success' => false,
                        'auth' => true
                    ];
                }
            } else {
                $category = new Category();
                //$category->user_id = $userId;
            }

            $category->setAttributes($data, false);
            $category->save(false);

            if ($category->ordered == 0) {
                $category->ordered = $category->id;
                $category->user_id = $userId;
                $category->save(false);
            }

            return [
                'success' => true,
                'alert' => [
                    'message' => 'Изменения сохранены !'
                ],
                'data' => $category->toArray()
            ];
        }

        return [
            'success' => false
        ];
    }


    public function actionDelete()
    {

        $userId = $this->getUserId();
        $request = Yii::$app->request;
        if ($request->isPost) {
            $data = Yii::$app->request->post();
            $id = isset($data['id'])? (int)$data['id']: 0;
            $category = Category::findOne($id);

            if(!$category) {
                return [
                    'success' => false,
                    'alert' => [
                        'message' => 'Нет такой категории',
                        'redirect' => '/',
                        'timeout' => 10
                    ]
                ];
            }

            if ($category->user_id == $userId) {
                $category->delete();
            } else {
                return [
                    'success' => false,
                    'auth' => true
                ];
            }

            return [
                'success' => true,
                'alert' => [
                    'message' => 'Категория успешно удалена !'
                ]
            ];
        }

        return [
            'success' => false
        ];
    }

    public function actionMove()
    {

        $request = Yii::$app->request;
        $userId = $this->getUserId();

        if ($request->isPost) {
            $data = Yii::$app->request->post();

            $id = isset($data['id'])?(int)$data['id']: 0;
            $direction = isset($data['direction'])?$data['direction']: '';


            $category = Category::findOne($id);

            if(!$category) {
                return [
                    'success' => false,
                    'alert' => [
                        'message' => 'Нет такой категории',
                        'redirect' => '/',
                        'timeout' => 10
                    ]
                ];
            }

            if($category->user_id != $userId) {
                return [
                    'success' => false,
                    'auth' => true
                ];
            }

            $category->move($direction);

            return $this->actionIndex();
        }

        return [
            'success' => false
        ];

    }
}
