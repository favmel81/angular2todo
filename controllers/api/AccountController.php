<?php

namespace app\controllers\api;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\HttpException;
use yii\helpers\Url;
use yii2utils\traits\CurrentUserIdTrait;
use yii2utils\services\UserManager;
use app\models\User;
use app\forms\CreateAccount;
use app\forms\EditAccount;
use app\forms\LoginForm;
use app\forms\ChangePassword;


class AccountController extends Controller
{

    use CurrentUserIdTrait;
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ]
            ],
            [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'get-user',
                            'check-login',
                            'check-email',
                            'logout'
                        ],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ],
                    [
                        'actions' => [
                            'check-edit-login',
                            'check-edit-email',
                            'edit-account-data',
                            'change-password',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                       [
                        'actions' => [
                            'login',
                            'create-account'
                        ],
                        'allow' => true,
                        'roles' => ['?'],
                    ]
                ],
                'denyCallback' => function ($rule, $action) {
                    switch ($action->id) {
                        case 'check-edit-login':
                        case 'check-edit-email':
                        case 'edit-account-data':
                        case 'change-password':
                        case 'login':
                        case 'create-account':
                            $data = [
                                'success' => false,
                                'auth' => true
                            ];
                            break;
                        default:
                            $data = [
                                'success' => false,
                                'auth' => false
                            ];
                    }

                    Yii::$app->getResponse()->data = $data;
                }
            ]
        ];

    }

    public function actionGetUser()
    {
        $user = Yii::$app->user->identity;

        if ($user) {
            unset(
                $user['password'],
                $user['auth_key'],
                $user['recovery_hash'],
                $user['recovery_time']
            );
        }

        $user = $user ? $user->toArray() : false;
        return [
            'success' => true,
            'data' => $user
        ];
    }

    public function actionCreateAccount()
    {
        $request = Yii::$app->request;
        if ($request->isPost) {
            $data = Yii::$app->request->post();
            $form = new CreateAccount();
            $form->setAttributes($data);
            if ($form->validate()) {
                $form->save(false);
                $userManager = new UserManager();
                $userManager->authById($form->id);
                return [
                    'success' => true,
                    'data' => Yii::$app->user->identity->toArray()
                ];
            } else {
                return [
                    'success' => false,
                    'errors' => $form->getErrors()
                ];
            }

        }
    }


    public function actionEditAccountData()
    {
        $request = Yii::$app->request;
        $form = EditAccount::findOne($this->getUserId());

        if ($request->isPost) {
            $data = Yii::$app->request->post();
            $fields = array_keys($data);

            $form->setAttributes($data, false);

            if($form->validate($fields)) {
                $form->save(false);
                return [
                    'success' => true,
                    'alert' => [
                        'message' => 'Данные сохранены'
                    ]
                ];
            }

        }

        return [
            'success' => false,
            'errors' => $form->getErrors()
        ];

    }


    public function actionChangePassword()
    {
        $request = Yii::$app->request;
        $form = ChangePassword::findOne($this->getUserId());

        if ($request->isPost) {
            $data = Yii::$app->request->post();
            $form->setAttributes($data);

            if ($form->validate()) {
                $form->savePassword();

                return [
                    'success' => true,
                    'alert' => [
                        'message' => 'Пароль успешно изменен'
                    ]
                ];
            }
        }

        return [
            'success' => false,
            'errors' => $form->getErrors()
        ];

    }


    public function actionLogin()
    {
        $form = new LoginForm();
        $request = Yii::$app->request;
        if ($request->isPost) {
            $form->setAttributes($request->post());

            if ($form->validate()) {
                $auth = $form->login();
                if ($auth === true) {
                    return [
                        'success' => true,
                        'data' => Yii::$app->user->identity->toArray()
                    ];
                }
            }

        }

        return [
            'success' => false,
            'errors' => $form->getErrors()
        ];
    }


    public function actionLogout()
    {
        Yii::$app->user->logout(true);
        return [
            'success' => true
        ];
    }


    public function actionCheckLogin()
    {
        $request = Yii::$app->request;
        if ($request->isPost) {
            $data = Yii::$app->request->post();
            $login = $data['login'];
            if (User::findByLogin($login)) {
                return [
                    'success' => false,
                    'error' => 'Такой Логин уже используется'
                ];
            } else {
                return [
                    'success' => true
                ];
            }
        }

    }

    public function actionCheckEditLogin()
    {
        $request = Yii::$app->request;
        $userId = $this->getUserId();
        if ($request->isPost) {
            $data = Yii::$app->request->post();
            $login = $data['login'];
            $user = User::findByLogin($login);
            if ($user && $user->id != $userId) {
                return [
                    'success' => false,
                    'error' => 'Такой Логин уже используется'
                ];
            }

            return [
                'success' => true
            ];
        }
    }

    public function actionCheckEmail()
    {
        $request = Yii::$app->request;
        if ($request->isPost) {
            $data = Yii::$app->request->post();
            $email = $data['email'];
            if (User::findByEmail($email)) {
                return [
                    'success' => false,
                    'error' => 'Такой Email уже используется'
                ];
            } else {
                return [
                    'success' => true
                ];
            }
        }
    }

    public function actionCheckEditEmail()
    {
        $request = Yii::$app->request;
        $userId = $this->getUserId();
        if ($request->isPost) {
            $data = Yii::$app->request->post();
            $email = $data['email'];
            $user = User::findByEmail($email);

            if ($user && $user->id != $userId) {
                return [
                    'success' => false,
                    'error' => 'Такой Email уже используется'
                ];
            }

            return [
                'success' => true
            ];

        }
    }

}
