<?php

namespace app\controllers\api;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii2utils\traits\CurrentUserIdTrait;
use app\models\Category;
use app\models\Task;


class TasksController extends Controller
{

    use CurrentUserIdTrait;

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ]
            ],
            [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'move',
                            'delete',
                            'save',
                            'change-status',
                            'get-all-by-category'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function($rule, $action) {
                    switch ($action->id) {
                        case 'move':
                        case 'delete':
                        case 'save':
                        case 'change-status':
                        case 'get-all-by-category':
                            $data = [
                                'success' => false,
                                'auth' => true
                            ];
                            break;
                        default:
                            $data = [
                                'success' => false,
                                'auth' => false
                            ];

                    }

                    Yii::$app->getResponse()->data = $data;
                }
            ]
        ];

    }

    public function actionGetAllByCategory($categoryId = null)
    {
        $request = Yii::$app->request;
        $userId = $this->getUserId();
        if ($categoryId > 0 || $request->isPost) {

            if ($categoryId === null) {
                $data = Yii::$app->request->post();
                $categoryId = isset($data['category'])?$data['category']: 0;
            }


            return [
                'success' => true,
                'data' => Task::find()->where(['category_id' => (int)$categoryId, 'user_id' => $userId])->orderBy(['ordered' => SORT_ASC])->asArray()->all()
            ];
        }
    }

    public function actionSave()
    {
        $request = Yii::$app->request;
        $userId = $this->getUserId();

        if ($request->isPost) {
            $data = Yii::$app->request->post();
            $id = isset($data['id'])?(int)$data['id']: 0;
            if ($id > 0) {
                $task = Task::findOne($id);

                if(!$task) {
                    return [
                        'success' => false,
                        'alert' => [
                            'message' => 'Нет такой задачи',
                            'redirect' => '/',
                            'timeout' => 10
                        ]
                    ];
                }

                if($task->user_id != $userId) {
                    return [
                        'success' => false,
                        'auth' => true
                    ];
                }
            } else {
                $task = new Task();
            }

            $task->setAttributes($data, false);
            $task->save(false);

            if ($task->ordered == 0) {
                $task->ordered = $task->id;
                $task->user_id = $userId;
                $task->save(false);
            }

            return [
                'success' => true,
                'alert' => [
                    'message' => 'Изменения сохранены !'
                ],
                'data' => $task->toArray()
            ];
        }

        return [
            'success' => false
        ];

    }


    public function actionDelete()
    {

        $request = Yii::$app->request;
        $userId = $this->getUserId();

        if ($request->isPost) {
            $data = Yii::$app->request->post();
            $id = isset($data['id'])?(int)$data['id']:0;
            $task = Task::findOne($id);

            if(!$task) {
                return [
                    'success' => false,
                    'alert' => [
                        'message' => 'Нет такой задачи',
                        'redirect' => '/',
                        'timeout' => 10
                    ]
                ];
            }

            if($task->user_id == $userId) {
                $task->delete();
            } else {
                return [
                    'success' => false,
                    'auth' => true
                ];
            }

            return [
                'success' => true,
                'alert' => [
                    'message' => 'Задача успешно удалена !'
                ]
            ];
        }
        return [
            'success' => false
        ];
    }

    public function actionMove()
    {

        $request = Yii::$app->request;
        $userId = $this->getUserId();

        if ($request->isPost) {
            $data = Yii::$app->request->post();
            $id = isset($data['id'])?(int)$data['id']: 0;
            $direction = isset($data['direction'])?$data['direction']: '';
            $categoryId = isset($data['category'])?(int)$data['category']: 0;

            $task = Task::findOne($id);

            if(!$task) {
                return [
                    'success' => false,
                    'alert' => [
                        'message' => 'Нет такой задачи',
                        'redirect' => '/',
                        'timeout' => 10
                    ]
                ];
            }

            if($task->user_id != $userId) {
                return [
                    'success' => false,
                    'auth' => true
                ];
            }

            $task->move($direction);
            return $this->actionGetAllByCategory($categoryId);
        }

        return [
            'success' => false
        ];

    }

    public function actionChangeStatus()
    {
        $userId = $this->getUserId();
        $request = Yii::$app->request;
        if ($request->isPost) {
            $data = Yii::$app->request->post();
            $id = isset($data['id'])?(int)$data['id']: 0;
            $task = Task::findOne($id);

            if(!$task) {
                return [
                    'success' => false,
                    'alert' => [
                        'message' => 'Нет такой задачи',
                        'redirect' => '/',
                        'timeout' => 10
                    ]
                ];
            }

            if ($task->user_id == $userId) {
                $task->changeStatus();
            } else {
                return [
                    'success' => false,
                    'auth' => true
                ];
            }

            return [
                'success' => true
            ];



        }
        return [
            'success' => false
        ];
    }


}
