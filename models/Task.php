<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;


class Task extends ActiveRecord
{
    const DIRECTION_UP = 'up';
    const DIRECTION_DOWN = 'down';

    public static function tableName() {
        return 'tasks';
    }

    public function move($direction) {

        if(!in_array($direction, [
            self::DIRECTION_DOWN, self::DIRECTION_UP
        ])) {
            return;
        }

        $ordered = $this->ordered;
        $condition = [$direction == self::DIRECTION_UP ? '<': '>', 'ordered', $ordered];


        $target = self::find()
            ->where($condition)
            ->andWhere(['user_id' => $this->user_id])
            ->orderBy(
                [
                    'ordered' => $direction == self::DIRECTION_UP ? SORT_DESC:SORT_ASC
                ]
            )
            ->limit(1)
            ->one();

        if($target) {
            $this->ordered = $target->ordered;
            $target->ordered = $ordered;
            $this->save();
            $target->save();
        }
    }

    public function changeStatus() {
        $this->done = (int)!$this->done;
        $this->save();
    }

}
