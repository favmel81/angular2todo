<?php


namespace app\models;

use Yii;
use yii2utils\web\UserIdentity;
use yii2utils\events\AfterUserMatchEvent;


class User extends UserIdentity
{
    const RECOVERY_LIFETIME_HOURS = 24;

    public function init() {
        parent::init();
        $this->on(self::EVENT_BEFORE_CHANGE_PASSWORD, [$this, 'beforeChangePassword']);
        $this->on(self::EVENT_AFTER_AUTH_MATCH_USER, [$this, 'afterAuthMatchUser']);

    }


    public static function tableName()
    {
        return 'users';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }


    public static function findByLogin($login) {
        return self::findOne(['login' => $login]);
    }

    public static function findByEmail($email) {
        return self::findOne(['email' => $email]);
    }

    public function generateRecoveryHash() {
        $hash = Yii::$app->security->generateRandomString(64);
        $this->recovery_hash = $hash;
        $this->recovery_time = time() + (self::RECOVERY_LIFETIME_HOURS + 1) * 3600;
        $this->save(false);
        return $hash;
    }

    public static function findByRecoveryHash($hash) {
        return self::find()->where(['and', ['=', 'recovery_hash', $hash], ['>=', 'recovery_time', time()]])->one();
    }

    public function beforeChangePassword() {
        $this->recovery_hash = null;
        $this->recovery_time = 0;
    }

    public function afterAuthMatchUser(AfterUserMatchEvent $event) {
        $user = $event->sender;
        if(!$user->active) {
            $event->addError('login', 'Пользователь заблокирован');
        }
    }

    public function getCountryName() {
        return Countries::findOne($this->country)['name_ru'];
    }




} 