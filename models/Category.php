<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;



class Category extends ActiveRecord
{
    const DIRECTION_UP = 'up';
    const DIRECTION_DOWN = 'down';


    public static function tableName()
    {
        return 'categories';
    }

    public function move($direction)
    {

        if (!in_array($direction, [
            self::DIRECTION_DOWN, self::DIRECTION_UP
        ])
        ) {
            return false;
        }



        $ordered = $this->ordered;
        $condition = [$direction == self::DIRECTION_UP ? '<' : '>', 'ordered', $ordered];


        $target = self::find()
            ->where($condition)
            ->andWhere(['user_id' => $this->user_id])
            ->orderBy(
                [
                    'ordered' => $direction == self::DIRECTION_UP ? SORT_DESC:SORT_ASC
                ]
            )
            ->limit(1)
            ->one();

        if ($target) {
            $this->ordered = $target->ordered;
            $target->ordered = $ordered;
            $this->save();
            $target->save();
            return true;
        }

        return false;
    }


    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Task::deleteAll([
                'category_id' => $this->id
            ]);
            return true;

        } else {
            return false;
        }
    }


}
