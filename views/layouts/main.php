<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

?>
<?php $this->beginPage() ?>
    <html>
    <head>
        <base href="/">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>


        <!-- 1. Load libraries -->


        <!-- Polyfill(s) for older browsers -->
        <script src="/node_modules/core-js/client/shim.min.js"></script>
        <script src="/node_modules/zone.js/dist/zone.js"></script>
        <script src="/node_modules/reflect-metadata/Reflect.js"></script>
        <script src="/node_modules/systemjs/dist/system.src.js"></script>


        <script src="/node_modules/jquery/dist/jquery.min.js"></script>
        <script src="/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>


        <!-- 2. Configure SystemJS -->
        <script src="/systemjs.config.js"></script>

        <link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/style.css">




        <?php $this->head() ?>
    </head>
    <body>

    <div class="container">
        <my-app>Loading...</my-app>
    </div>

    <script>
        System.import('app').then(null, console.error.bind(console));
    </script>

    <?php $this->beginBody() ?>
    <?= $content ?>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>