<?php


// comment out the following two lines when deployed to production
defined('YII_ENV') || define('YII_ENV', getenv('APPLICATION_ENV')?:'dev');
defined('YII_DEBUG') or define('YII_DEBUG', YII_ENV != 'prod');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');




$app = new yii2utils\web\Application($config);
Yii::$app->on(yii2utils\web\Application::EVENT_RESOLVE_ROUTE, function() {
    if(strpos(Yii::$app->requestedRoute, 'api/') === 0) {
        Yii::$app->requestedRoute = str_replace('api/', '', Yii::$app->requestedRoute);
        //Yii::$app->requestedRoute = 'categories/index';
        Yii::$app->controllerNamespace = 'app\\controllers\\api';

    }
});


$app->run();