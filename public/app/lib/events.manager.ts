import {Injectable} from '@angular/core';
import {EventEmitter} from '@angular/core/src/facade/async';



@Injectable()
export class EventsManager {

    protected emitters = {};

    getEmitter(name: string): EventEmitter<any> {
        if(!this.emitters[name]) {
            this.emitters[name] = new EventEmitter();
        }
        return this.emitters[name]
    }
}
