import {Component, ElementRef, OnInit, AfterViewInit, Injectable} from '@angular/core';
import {EventEmitter} from '@angular/core/src/facade/async';


declare var jQuery:any;

@Component({
    selector: 'confirm-window',
    templateUrl: 'app/lib/ui/confirm/confirm.window.html'
})
@Injectable()
export class ConfirmWindow extends EventEmitter<any> implements AfterViewInit {

    static CONFIRM_STATUS_OK = 'OK';
    static CONFIRM_STATUS_CANCEL = 'CANCEL';

    element:any;
    title:string;
    content:string;
    pushedOk:boolean;


    constructor(public elementRef:ElementRef) {
        super();
    }


    ngAfterViewInit() {
        var that = this;
        this.element = jQuery(this.elementRef.nativeElement).children(':first');
        this.element.on('hide.bs.modal', function () {

            if (!that.pushedOk) {
                that.emit(ConfirmWindow.CONFIRM_STATUS_CANCEL);
            }

            that.pushedOk = false;
        });
    }


    show(title:string, content:string) {
        this.setTitle(title);
        this.setContent(content);
        this.element.modal('show');
        return this;
    }



    confirmOK() {
        this.pushedOk = true;
        this.emit(ConfirmWindow.CONFIRM_STATUS_OK);
        this.element.modal('hide');
    }

    setTitle(title:string) {
        this.title = title;
    }

    setContent(content:string) {
        this.content = content;
    }


};