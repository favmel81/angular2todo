import {Component, ElementRef, OnInit, AfterViewInit, Injectable} from '@angular/core';
import {EventEmitter} from '@angular/core/src/facade/async';


declare var jQuery:any;

@Component({
    selector: 'alert-window',
    templateUrl: 'app/lib/ui/alert/alert.window.html'
})
@Injectable()
export class AlertWindow extends EventEmitter<any> implements AfterViewInit {

    static CONFIRM_STATUS_OK = 'OK';

    element:any;
    title:string = null;
    content:string;
    downcount:number = 10;


    constructor(public elementRef:ElementRef) {
        super();
    }


    ngAfterViewInit() {
        var that = this;
        this.element = jQuery(this.elementRef.nativeElement).children(':first');
        /*this.element.on('hide.bs.modal', function () {

         if (!that.pushedOk) {
         that.emit(ConfirmWindow.CONFIRM_STATUS_CANCEL);
         }

         that.pushedOk = false;
         });*/
    }


    show(config) {

        var message = config.message || '',
            redirect = config.redirect || false,
            downcount = config.timeout || null,
            that = this;

        that.setDowncount(downcount);
        that.setContent(message);

        if (redirect) {

            var subscriber = that.subscribe(() => {
                that.element.off('hide.bs.modal');
                subscriber.unsubscribe();
                document.location.href = redirect;
            });

            that.element.on('hide.bs.modal', function () {
                that.emit(null);
            });

        }



        if (this.downcount !== null && this.downcount >= 0) {
            var intervalId = setInterval(function() {
                that.setDowncount(--that.downcount);
                if(that.downcount < 0) {
                    clearInterval(intervalId);
                    that.ok();
                }

            }, 1000);
        }

        this.element.modal('show');
        return this;
    }


    ok() {
        this.element.modal('hide');
    }

    setTitle(title:string) {
        this.title = title;
    }

    setContent(content:string) {
        this.content = content;
    }

    getDowncount() {
        if (this.downcount !== null && this.downcount >= 0) {
            return ' (' + this.downcount + ' s.)';
        }
    }

    setDowncount(seconds:number = null) {
        this.downcount = seconds;
    }


}