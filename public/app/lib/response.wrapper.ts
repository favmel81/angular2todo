export class ResponseWrapper {
    success: boolean = false;
    data: any = null;
    errors: any = null;
    error: string;

    constructor(data: any) {
        this.success = data.success || false;
        this.data = data.data || null;
        this.errors = data.errors || null;
        this.error = data.error || null;
    }
}
