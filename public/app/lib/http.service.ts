import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable}     from 'rxjs/Observable';
import {EventsManager} from '../lib/events.manager';
import {ResponseWrapper} from './response.wrapper';


@Injectable()
export class HttpService {

    static EVENT_BEFORE_EXTRACT_DATA = 'event_before_extract_data';
    static EVENT_AFTER_EXTRACT_DATA = 'event_after_extract_data';
    static EVENT_ON_SUCCESS_EXTRACT_JSON = 'event_on_success_extract_json';
    static EVENT_ON_FAIL_EXTRACT_JSON = 'event_on_fail_extract_json';
    static EVENT_ON_SERVER_ERROR = 'event_on_server_error';


    public http:Http;
    public static events:EventsManager;


    constructor(http:Http, events:EventsManager) {
        this.http = http;
        if (!HttpService.events) {
            HttpService.events = events;
        }
    }


    public extractData(res:Response) {
        /*if (res.status < 200 || res.status >= 300) {
         throw new Error('Bad response status: ' + res.status);
         }*/

        HttpService.events.getEmitter(HttpService.EVENT_BEFORE_EXTRACT_DATA).emit(res);

        let data;
        try {
            data = res.json();
            data.success = data.success || false;
            data.status = data.status || res.status;
        } catch (error) {
            data = {
                success: false,
                status: res.status,
                error: 'Ошибка сервера !'
            };
        }

        HttpService.events.getEmitter(HttpService.EVENT_AFTER_EXTRACT_DATA).emit(data);

        return new ResponseWrapper(data);
    }


    public handleError(error:any) {
        let data;
        try {
            data = JSON.parse(error._body);
            data.success = data.success || false;
            data.status = data.status || error.status;
        } catch (e) {
            data = {
                success: false,
                status: error.status,
                error: error._body || error.message || 'Server error'
            };
        }
        HttpService.events.getEmitter(HttpService.EVENT_AFTER_EXTRACT_DATA).emit(data);
        return Observable.throw(error.message || 'Server error');
    }


}

