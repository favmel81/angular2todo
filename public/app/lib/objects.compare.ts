export function ObjectsCompare(object1, object2) {

    if(object1 === null && object2 === null) {
        return true;
    }



    if(object1 === null || object2 === null) {
        return false;
    }


    var ownPropertiesCount = (obj) => {
        var length = 0;

        for(var i in obj) {
            if(obj.hasOwnProperty(i)) {
                length++;
            }
        }
        return length;
    };


    if(ownPropertiesCount(object1) !== ownPropertiesCount(object2)) {
        return false;
    }

    for(var index in object1) {

        if (!object1.hasOwnProperty(index)) {
            continue;
        }

        if(!object2.hasOwnProperty(index) || object1[index] !== object2[index]) {
            return false;
        }
    }

    return true;
}