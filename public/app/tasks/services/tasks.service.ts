import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable}     from 'rxjs/Observable';
import {HttpService} from '../../lib/http.service';
import {EventsManager} from '../../lib/events.manager';
import {ResponseWrapper} from '../../lib/response.wrapper';
import {TaskModel} from '../models/task.model';


@Injectable()
export class TasksService extends HttpService {

    private url:string = '/api/tasks';

    constructor(http:Http, public events: EventsManager) {
        super(http, events);
    }

    getAllByCategory(categoryId: number): Observable<ResponseWrapper> {

        let body = JSON.stringify({category: categoryId});
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/get-all-by-category', body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    save(item: TaskModel): Observable<ResponseWrapper> {

        let body = JSON.stringify(item);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/save', body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    delete(id: number): Observable<ResponseWrapper> {

        let body = JSON.stringify({id: id});
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/delete', body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    move(item: TaskModel, direction: string, categoryId: number): Observable<ResponseWrapper> {

        let body = JSON.stringify({id: item.id, direction: direction, category: categoryId});
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/move', body, options)
            .map(this.extractData)
            .catch(this.handleError);

    }


    changeStatus(item: TaskModel): Observable<ResponseWrapper> {
        let body = JSON.stringify({id: item.id});
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/change-status', body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

}
