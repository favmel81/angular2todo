import {Component, Input, Output} from '@angular/core';
import {EventEmitter} from '@angular/core/src/facade/async';
import {TaskModel} from './models/task.model';





@Component({
    selector: 'tasks-filter',
    templateUrl: 'app/tasks/tasks.filter.component.html',
})
export class TasksFilterComponent {
    @Input()
    public tasks: Array<TaskModel> = null;

    @Output()
    public applyFilter = new EventEmitter();

    protected currentFilter: string = 'all';

    getCount(type: string) {
        var total = 0, done = 0, undone = 0;

        if(this.tasks) {
            this.tasks.forEach((item, index) => {
                total++;
                if(item.done > 0) {
                    done++;
                } else {
                    undone++;
                }
            })
        }

        if(type == 'all') {
            return total;
        }

        if(type == 'done') {
            return done;
        }

        return undone;
    }

    filter(type) {
        this.currentFilter = type;
        this.applyFilter.emit(type);
        return false;
    }




}
