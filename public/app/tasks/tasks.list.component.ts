import {Component, OnInit, OnDestroy, Host, Inject, forwardRef} from '@angular/core';
import {ControlGroup, Control, Validator, Validators, FormBuilder, FORM_DIRECTIVES} from '@angular/common';
import {CanReuse} from '@angular/router-deprecated';
import {isArray} from "rxjs/util/isArray";
import {EventsManager} from '../lib/events.manager';
import {AppComponent} from '../app.component';
import {TaskModel} from './models/task.model';
import {UserService} from '../common/services/user.service';
import {CategoriesService} from '../categories/services/categories.service';
import {TasksService} from '../tasks/services/tasks.service';
import {CategoryModel} from "../categories/models/category.model";
import {ConfirmWindow} from "../lib/ui/confirm/confirm.window";
import {CommonFormValidators} from '../common/validators/common.form.validators';
import {TasksFilterComponent} from './tasks.filter.component';



@Component(
    {
        templateUrl: 'app/tasks/tasks.list.component.html',
        directives: [
            FORM_DIRECTIVES,
            TasksFilterComponent
        ]
    }
)
export class TasksListComponent implements OnInit, OnDestroy, CanReuse {

    selectedCategoryId:number;
    categories:Array<CategoryModel> = [];
    tasks:Array<TaskModel> = [];
    public edit = null;
    private oldName:string;
    public isUser:boolean = false;
    protected subscriptions = [];
    public app: AppComponent;
    public form;
    protected currentFilter: string = 'all';


    constructor( @Host() @Inject(forwardRef(() => AppComponent)) app: AppComponent,
                 public fb:FormBuilder,
                 protected _categoriesService:CategoriesService,
                protected _tasksService:TasksService,
                protected user:UserService,
                protected events:EventsManager
    ) {
        this.app = app;
        this.createAddTaskForm();
    }


    ngOnInit() {
        var subscription = this.events.getEmitter(UserService.EVENT_USER_UPDATED).subscribe(event => {
            var user = event.user;
            this.isUser = user.user ? true : false;
            this.loadCategories();
        });

        this.subscriptions.push(subscription);
        this.user.sync();
    }

    ngOnDestroy() {
        this.subscriptions.forEach(item => item.unsubscribe());
        this.subscriptions = [];
    }


    loadCategories() {
        this.selectedCategoryId = null;
        this._categoriesService.getAll().subscribe(
            data => {
                let categories = [];
                if (data.success && data.data) {

                    data.data.forEach((item, index) => {
                        categories.push({
                            id: item.id,
                            text: item.name
                        });
                    });
                }
                this.categories = categories;
            },
            error => console.log(error)
        );

    }

    doAction(action) {

        switch (action) {
            case 'login':
                this.events.getEmitter(AppComponent.EVENT_SHOW_LOGIN_FORM).emit(null);
                break;

            case 'register':
                this.events.getEmitter(AppComponent.EVENT_SHOW_REGISTER_FORM).emit(null);
                break;
        }

        return false;
    }

    routerCanReuse() {
        return false;
    }


    selectCategory(event) {
        this.selectedCategoryId = event.target.value;
        this.getTasks();
    }

    getTasks() {
        this._tasksService.getAllByCategory(this.selectedCategoryId).subscribe(data => {
            let tasks = [];
            if (data.success && data.data) {
                tasks = data.data;
            }
             this.tasks = tasks;

        });
    }

    hasTasks() {
        return isArray(this.tasks) && this.tasks.length;
    }

    getFilteredTasks() {
        if(this.hasTasks()) {
            return this.tasks.filter((item) => {
                if(this.currentFilter == 'all') {
                    return true;
                }

                if(item.done > 0) {
                    if(this.currentFilter == 'done') {
                        return true;
                    }
                } else {
                    if(this.currentFilter == 'undone') {
                        return true;
                    }
                }
            })

        }
    }


    filter(type) {
        this.currentFilter = type;
    }

    deleteTask(item:TaskModel) {


        var subscriber = this.app.getConfirmWindow().show('Удаление задачи', 'Удалить задачу "' + item.name + '" ?').subscribe(status => {


            if(status == ConfirmWindow.CONFIRM_STATUS_OK) {
                this._tasksService.delete(item.id).subscribe(data => {
                    if (data.success) {
                        let c = this.tasks.length,
                            _list = [];
                        if (c) {

                            for (var i = 0; i < c; i++) {
                                if (this.tasks[i] === item) {
                                    continue;
                                }
                                _list.push(this.tasks[i]);
                            }

                            this.tasks = _list;
                        }
                    }
                });
            }

            subscriber.unsubscribe();
        });

    }

    addTask() {
        let input = this.form.controls.name;
        if (input.valid) {

            this._tasksService.save(
                new TaskModel(0, input.value, 0, this.selectedCategoryId)
            ).subscribe(data => {
                if (data.success && data.data) {
                    if (!isArray(this.tasks)) {
                        this.tasks = [];
                    }
                    this.tasks.push(data.data);
                    this.createAddTaskForm();
                }
            });
        }
    }

    changeStatus(item:TaskModel) {
        this._tasksService.changeStatus(item).subscribe(data => {
            if (data.success) {
                console.log(item.done);
                item.done = (!  (item.done*1) ) ? 1 : 0
                console.log(item.done);
            }
        });
    }

    startEditName(item:TaskModel) {
        if (this.edit === null) {
            this.edit = item;
            this.oldName = item.name;
        }
    }

    cancelEdit() {
        this.edit.name = this.oldName;
        this.edit = null;
        this.oldName = '';
    }


    finishEditName(event) {
        var newName = this.edit.name.trim();

        if(event === true || (event.keyCode != undefined && event.keyCode == 13)) {

            if (newName == '' || newName == this.oldName) {
                this.edit.name = this.oldName;
                this.edit = null;
                this.oldName = '';
                return;
            }

            this._tasksService.save(this.edit).subscribe(data => {
                if (data.success) {
                    this.edit = null;
                    this.oldName = '';
                }
            });
        }

    }

    move(item, direction) {
        this._tasksService.move(item, direction, this.selectedCategoryId).subscribe(
            data => {
                if (data.success && data.data) {
                    this.tasks = data.data;
                }
            },
            error => console.log(error)
        )
    }

    createAddTaskForm() {
        this.form = this.fb.group({
            'name': ['', CommonFormValidators.notEmptyValidator('Введите название задачи')],
        });
    }


}

