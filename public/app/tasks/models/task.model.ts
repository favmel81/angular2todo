export class TaskModel {
    public id:number;
    public name:string;
    public done:number;
    public category_id:number;
    public ordered:number;
    public user_id:number;

    public constructor(id:number = 0, name:string, done:number, category_id:number, ordered:number = 0, user_id:number = 0) {
        this.id = id;
        this.name = name;
        this.done = done;
        this.category_id = category_id;
        this.ordered = ordered;
        this.user_id = user_id;
    }
}