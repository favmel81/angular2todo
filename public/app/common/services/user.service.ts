import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable}     from 'rxjs/Observable';
import {EventEmitter} from '@angular/core/src/facade/async';
import {HttpService} from '../../lib/http.service';
import {EventsManager} from '../../lib/events.manager';
import {ObjectsCompare} from '../../lib/objects.compare';


@Injectable()
export class UserService extends HttpService {
    public user = null;

    static EVENT_USER_UPDATED:string = 'event_user_updated';

    constructor(http:Http, public events:EventsManager) {
        super(http, events);
    }


    setUser(user, emit: boolean = true) {
        this.user = user;

        if(emit) {
            this.events.getEmitter(UserService.EVENT_USER_UPDATED).emit({
                user: this,
                changed: !ObjectsCompare(this.user, user)
            });
        }

    }

    resetUser() {
        this.setUser(null);
    }


    sync():Promise<UserService> {
        return new Promise(resolve => {

            let body = JSON.stringify({});
            let headers = new Headers({'Content-Type': 'application/json'});
            let options = new RequestOptions({headers: headers});

            this.http.post('/api/account/get-user', body, options)
                .map(this.extractData)
                .catch(this.handleError)
                .subscribe(data => {
                    let user = null;
                    if (data.success && data.data) {
                        user = data.data;
                    }
                    this.setUser(user);
                    resolve(this);
                })
        });
    }
}
