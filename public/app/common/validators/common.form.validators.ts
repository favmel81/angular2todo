import {Control, ControlGroup} from '@angular/common';


export class CommonFormValidators {


    static isCheckedValidator(error: string = null) {
        return (control: Control) => {
            if(control.value) {
                return null;
            }

            return {
                error: error !== null? error: true
            }
        }
    }




    static notEmptyValidator(error: string = null) {
        return (control:Control) => {
            let value:string = control.value.trim();

            if (value === '') {
                return {
                    error: error !== null? error: true
                };
            }
            return null;
        }
    }


}
