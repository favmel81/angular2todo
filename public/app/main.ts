import {bootstrap}    from '@angular/platform-browser-dynamic';
import { HTTP_PROVIDERS } from '@angular/http';
import { ROUTER_PROVIDERS } from '@angular/router-deprecated';

import 'rxjs/Rx';
import {EventsManager} from './lib/events.manager';
import {AppComponent} from './app.component';
import {AccountService} from './account/services/account.service';
import {UserService} from './common/services/user.service';


import {enableProdMode} from '@angular/core';

enableProdMode();

bootstrap(AppComponent,
    [
        HTTP_PROVIDERS,
        ROUTER_PROVIDERS,
        AccountService,
        UserService,
        EventsManager
    ]
);