import {Control, ControlGroup} from '@angular/common';
import {AccountService} from '../../services/account.service';


export class FormValidators {


    static createLoginValidator(accountService:AccountService, registerForm: boolean = true) {
        return (control:Control):{[key: string]: any} => {
            return new Promise(resolve => {
                let value:string = control.value.trim();
                if (value === '') {
                    resolve({
                        error: 'Поле Логин не может быть пустым!'
                    });
                } else {
                    /*if (value.length < 3) {
                        return resolve({
                            error: 'Поле Логин должно содержать не менее 3 символов!'
                        })

                    }*/

                    var pattern = /^[a-zA-Z]+[a-zA-Z0-9_]*$/i;
                    if (!pattern.test(value)) {
                        return resolve({
                            error: 'Поле Логин содержит неверные символы!'
                        })
                    }

                    var method = registerForm? 'checkRegisterLogin': 'checkEditLogin';

                    //Проверяем данные на сервере
                    accountService[method](value).subscribe(data => {

                        if(data.success) {
                            return resolve(null);
                        }

                        if (data.error) {
                            return resolve({
                                error: data.error
                            });
                        }

                    });
                }

            });
        }
    }


    static createEmailValidator(accountService:AccountService, registerForm: boolean = true) {
        return (control:Control):{[key: string]: any} => {
            return new Promise(resolve => {
                let value:string = control.value.trim();

                if (value === '') {
                    resolve({
                        error: 'Поле Email не может быть пустым!'
                    });
                } else {

                    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (!pattern.test(value)) {
                        return resolve({
                            error: 'Поле Email содержит неверные символы!'
                        })
                    }

                    var method = registerForm? 'checkRegisterEmail': 'checkEditEmail';

                    //Проверяем данные на сервере
                    accountService[method](value).subscribe(data => {
                        if(data.success) {
                            return resolve(null);
                        }

                        if (data.error) {
                            return resolve({
                                error: data.error
                            });
                        }
                    });
                }

            });
        }
    }



    static createPasswordWithConfirmValidator(form: ControlGroup, name: string) {
        return (control: Control):{[key: string]: any} => {


            var password = form.find('password'),
                password1 = form.find('password1');

            if(password == null || password1 == null) {
                return;
            }

                var passwordValue = password.value.trim(),
                password1Value = password1.value.trim();

            if(name == 'password') {
                if(passwordValue == '') {
                  return {
                        error: 'Введите пароль'
                    };
                }

                if(password1Value != '') {

                    if(passwordValue !== password1Value) {
                        password1.setErrors({
                            error: 'Проверьте правильность ввода паролей'
                        });

                        return {
                            error: 'Пароли должны совпадать'
                        };
                    }

                    password1.setErrors(null);


                    return null;



                }




            } else {

                if(password1Value == '') {
                    return {
                        error: 'Введите подтверждение пароля'
                    };
                }

                if(passwordValue != '') {

                    if(passwordValue !== password1Value) {
                        password.setErrors({
                            error: 'Пароли должны совпадать'
                        });

                        return {
                            error: 'Проверьте правильность ввода паролей'
                        };
                    }

                    password.setErrors(null);

                   return null;

                }


            }


        }
    }
}
