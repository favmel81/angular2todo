import {Component, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import {ControlGroup, Control, Validator, Validators, FormBuilder, FORM_DIRECTIVES} from '@angular/common';
import { Router } from '@angular/router-deprecated';
import {FormValidators} from './validators/form.validators';
import {CommonFormValidators} from '../../common/validators/common.form.validators';
import {AccountService} from '../services/account.service';
import {UserService} from '../../common/services/user.service';


declare var jQuery:any;

@Component({
    selector: 'change-password-form',
    templateUrl: 'app/account/forms/change-password.form.html',
    directives: [
        FORM_DIRECTIVES
    ]
})
export class ChangePasswordForm implements  AfterViewInit  {

    element: any;
    form: ControlGroup;

    constructor(public elementRef: ElementRef, public fb: FormBuilder, protected accountService: AccountService, protected user: UserService, protected router: Router) {
        this.createForm();
    }


    ngAfterViewInit() {
        var that = this;
        that.element = jQuery(this.elementRef.nativeElement).children(':first');

        that.element.on('hide.bs.modal', function () {
            that.createForm();
        });
    }


    show() {
        this.element.modal('show');
    }

    hide() {
        this.element.modal('hide');
    }

    submit() {
        let form = this.form;
        if(!form.valid || form.pending) {
            return;
        }

        this.accountService.changePassword(form.value).subscribe(data => {
            if(data.success) {
                this.hide();
            } else if(data.errors) {
                for(var index in data.errors) {
                    if (data.errors.hasOwnProperty(index)) {
                        var control = this.form.find(index);
                        if(control) {
                            //noinspection TypeScriptValidateTypes
                            control.setErrors({
                                error: data.errors[index][0]
                            });
                        }

                    }
                }
            }
        })
    }


    createForm() {

        var form = this.fb.group({
            'password': ['', CommonFormValidators.notEmptyValidator('Введите пароль')],
            'password1': ['', CommonFormValidators.notEmptyValidator('Введите повтор пароля')]
        });

        form.addControl('password', new Control('', FormValidators.createPasswordWithConfirmValidator(form, 'password')));
        form.addControl('password1', new Control('', FormValidators.createPasswordWithConfirmValidator(form, 'password1')));

        this.form = form;
    }

}
