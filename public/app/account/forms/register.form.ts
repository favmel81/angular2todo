import {Component, ElementRef, AfterViewInit } from '@angular/core';
import {ControlGroup, Control, Validator, Validators, FormBuilder, FORM_DIRECTIVES} from '@angular/common';
import { Router } from '@angular/router-deprecated';
import {FormValidators} from './validators/form.validators';
import {CommonFormValidators} from '../../common/validators/common.form.validators';
import {AccountService} from '../services/account.service';

declare var jQuery:any;

@Component({
    selector: 'register-form',
    templateUrl: 'app/account/forms/register.form.html',
    directives: [
        FORM_DIRECTIVES
    ]
})
export class RegisterForm implements  AfterViewInit  {

    element: any;
    form: ControlGroup;

    constructor(public elementRef: ElementRef, public fb: FormBuilder, protected accountService: AccountService, protected router: Router) {
        this.createForm();
    }


    ngAfterViewInit() {
        var that = this;
        that.element = jQuery(this.elementRef.nativeElement).children(':first');

        that.element.on('hide.bs.modal', function () {
            that.createForm();
        });
    }



    show() {
        this.element.modal('show');
    }

    hide() {
        this.element.modal('hide');
    }

    submit() {
        let form = this.form;
        if(!form.valid || form.pending) {
            return;
        }

        this.accountService.createAccount(form.value).subscribe(data => {


            if(data.success) {
                //this.user.setUser(data.data);
                this.hide();
                this.createForm();
                this.router.navigate(['Home']);
            } else if(data.errors) {
                for(var index in data.errors) {
                    if (data.errors.hasOwnProperty(index)) {
                        var control = this.form.find(index);
                        if(control) {
                            control.setErrors({
                                error: data.errors[index][0]
                            })
                        }
                    }
                }
            }
        })
    }


    createForm() {
        var form = this.fb.group({
            'login': ['', ,FormValidators.createLoginValidator(this.accountService)],
            'email': ['', ,FormValidators.createEmailValidator(this.accountService)],
            'lastname': ['', CommonFormValidators.notEmptyValidator('Введите фамилию')],
            'firstname': ['', CommonFormValidators.notEmptyValidator('Ведите имя')],
            'middlename': ['', CommonFormValidators.notEmptyValidator('Ведите отчество')],
            'address': [''],
            'confirm': [0, CommonFormValidators.isCheckedValidator('Вы должны согласиться с условиями')]
        });

        form.addControl('password', new Control('', FormValidators.createPasswordWithConfirmValidator(form, 'password')));
        form.addControl('password1', new Control('', FormValidators.createPasswordWithConfirmValidator(form, 'password1')));
        this.form = form;
    }

}
