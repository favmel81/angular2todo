import {Component, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import {ControlGroup, Control, Validator, Validators, FormBuilder, FORM_DIRECTIVES} from '@angular/common';
import { Router } from '@angular/router-deprecated';
import {FormValidators} from './validators/form.validators';
import {CommonFormValidators} from '../../common/validators/common.form.validators';
import {AccountService} from '../services/account.service';
import {UserService} from '../../common/services/user.service';


declare var jQuery:any;

@Component({
    selector: 'auth-form',
    templateUrl: 'app/account/forms/auth.form.html',
    directives: [
        FORM_DIRECTIVES
    ]
})
export class AuthForm implements  AfterViewInit  {

    element: any;
    form: ControlGroup;


    constructor(public elementRef: ElementRef, public fb: FormBuilder, protected accountService: AccountService, protected user: UserService, protected router: Router) {
        this.createForm();
    }


    ngAfterViewInit() {
        var that = this;
        that.element = jQuery(this.elementRef.nativeElement).children(':first');

        that.element.on('hide.bs.modal', function () {
            that.createForm();
        });
    }


    show() {
        this.element.modal('show');
    }

    hide() {
        this.element.modal('hide');
    }

    submit() {
        let form = this.form;
        if(!form.valid || form.pending) {
            return;
        }

        this.accountService.login(form.value).subscribe(data => {
            if(data.success && data.data) {
                this.hide();
                this.user.setUser(data.data, false);
                this.router.navigate(['Account']);
            } else if(data.errors) {
                for(var index in data.errors) {
                    if (data.errors.hasOwnProperty(index)) {
                        var control = this.form.find(index);
                        if(control) {
                            //noinspection TypeScriptValidateTypes
                            control.setErrors({
                                error: data.errors[index][0]
                            });
                        }

                    }
                }
            }
        })
    }


    createForm() {
        this.form = this.fb.group({
            'login': ['', CommonFormValidators.notEmptyValidator('Введите логин')],
            'password': ['', CommonFormValidators.notEmptyValidator('Введите пароль')],
            'remember': [0]
        });
    }

}
