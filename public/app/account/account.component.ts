import {Component, ViewChild, OnInit, OnDestroy} from '@angular/core';
import {ControlGroup, Control, Validator, Validators, FormBuilder, FORM_DIRECTIVES} from '@angular/common';

import { Router } from '@angular/router-deprecated';
import {CanReuse} from '@angular/router-deprecated';
import {AccountService} from './services/account.service';
import {UserService} from '../common/services/user.service';
import {EventsManager} from '../lib/events.manager';
import {FormValidators} from './forms/validators/form.validators';
import {CommonFormValidators} from '../common/validators/common.form.validators';
import {ChangePasswordForm} from './forms/change-password.form';


@Component({
    templateUrl: 'app/account/account.component.html',
    directives: [
        FORM_DIRECTIVES,
        ChangePasswordForm
    ]
})
export class AccountComponent implements OnInit, OnDestroy, CanReuse {

    protected subscriptions = [];
    public userData = null;
    public form;
    protected edit = null;
    protected oldValue = '';

    @ViewChild(ChangePasswordForm) changePasswordForm: ChangePasswordForm;


    constructor(protected router:Router,
                public fb:FormBuilder,
                protected user:UserService,
                protected accountService: AccountService,
                protected events:EventsManager) {

        /*if (!this.user.user) {
            this.router.navigate(['Home']);
        }*/

//@todo need update single fiel optimization
        var subscription = this.events.getEmitter(UserService.EVENT_USER_UPDATED).subscribe(event => {

            if (!event.user.user) {
                return this.router.navigate(['Home']);
            }


            this.userData = event.user.user;
            this.createForm();
        });

        this.subscriptions.push(subscription);
        this.user.sync();
    }

    ngOnInit() {

    }

    createForm() {

        this.form = this.fb.group({
            'login': [this.userData.login, ,FormValidators.createLoginValidator(this.accountService, false)],
            'email': [this.userData.email, ,FormValidators.createEmailValidator(this.accountService, false)],
            'lastname': [this.userData.lastname, CommonFormValidators.notEmptyValidator('Введите фамилию')],
            'firstname': [this.userData.firstname, CommonFormValidators.notEmptyValidator('Введите имя')],
            'middlename': [this.userData.middlename, CommonFormValidators.notEmptyValidator('Введите отчество')],
            'address': [this.userData.address]
        });

    }

    startEdit(name) {
        this.edit = name;
        this.oldValue = this.form.find(name).value;
    }


    cancelEdit() {
        this.form.find(this.edit).updateValue(this.oldValue);
        this.oldValue = '';
        this.edit = null;
    }

    finishEdit(event) {


        if(!(event === true || (event.keyCode != undefined && event.keyCode == 13))) {
            return;
        }

        var control = this.form.find(this.edit),
            name = this.edit,
            data = {},
            value = control.value.trim();

        data[name] = value;

        if(this.oldValue == value) {
            this.oldValue = '';
            this.edit = null;
            return;
        }

        this.accountService.editData(data).subscribe(data => {
            if(data.success) {
                this.user.sync();
                this.oldValue = '';
                this.edit = null;
            } else {
                if(data.errors) {
                    for(var index in data.errors) {
                        if (data.errors.hasOwnProperty(index)) {
                            var econtrol = this.form.find(index);
                            if(econtrol) {
                                econtrol.setErrors({
                                    error: data.errors[index][0]
                                })
                            }
                        }
                    }
                }

            }
        });
    }

    showChangePasswordForm() {
        this.changePasswordForm.show();
        return false;
    }

    routerCanReuse() {
        return false;
    }

    ngOnDestroy() {
        this.subscriptions.forEach(item => item.unsubscribe());
        this.subscriptions = [];
    }



}
