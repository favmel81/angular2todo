import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable}     from 'rxjs/Observable';
import {EventsManager} from '../../lib/events.manager';
import {ResponseWrapper} from '../../lib/response.wrapper';
import {HttpService} from '../../lib/http.service';




@Injectable()
export class AccountService extends HttpService {

    private url:string = '/api/account';

    constructor(http:Http, events: EventsManager) {
        super(http, events);
    }

    checkRegisterLogin(value: string): Observable<ResponseWrapper> {

        let body = JSON.stringify({login: value});
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/check-login', body, options)
            .map(this.extractData)
            .catch(this.handleError);

    }

    checkEditLogin(value: string): Observable<ResponseWrapper> {

        let body = JSON.stringify({login: value});
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/check-edit-login', body, options)
            .map(this.extractData)
            .catch(this.handleError);

    }

    checkRegisterEmail(value: string): Observable<ResponseWrapper> {

        let body = JSON.stringify({email: value});
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/check-email', body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    checkEditEmail(value: string): Observable<ResponseWrapper> {

        let body = JSON.stringify({email: value});
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/check-edit-email', body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    editData(data: Object): Observable<ResponseWrapper> {
        let body = JSON.stringify(data);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/edit-account-data', body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    changePassword(data: Object): Observable<ResponseWrapper> {
        let body = JSON.stringify(data);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/change-password', body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }


    createAccount(data: Object): Observable<ResponseWrapper> {
        let body = JSON.stringify(data);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/create-account', body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    login(data: Object): Observable<ResponseWrapper> {
        let body = JSON.stringify(data);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/login', body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    logout(): Observable<ResponseWrapper>  {
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });

            return this.http.get('/api/account/logout', options)
                .map(this.extractData)
                .catch(this.handleError);
    }

}