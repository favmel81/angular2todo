import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable}     from 'rxjs/Observable';
import {HttpService} from '../../lib/http.service';
import {EventsManager} from '../../lib/events.manager';
import {ResponseWrapper} from '../../lib/response.wrapper';
import {CategoryModel} from '../models/category.model';


@Injectable()
export class CategoriesService extends HttpService{

    private url:string = '/api/categories';


    constructor(http:Http, events: EventsManager) {
        super(http, events);
    }

    getAll(): Observable<ResponseWrapper> {
        return this.http.get(this.url).map(this.extractData).catch(this.handleError);
    }

    save(item: CategoryModel):Observable<ResponseWrapper> {

        let body = JSON.stringify(item);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/save', body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    delete(id: number):Observable<ResponseWrapper> {

        let body = JSON.stringify({id: id});
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/delete', body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    move(item: CategoryModel, direction: string):Observable<ResponseWrapper> {

        let body = JSON.stringify({id: item.id, direction: direction});
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url + '/move', body, options)
            .map(this.extractData)
            .catch(this.handleError);

    }


}
