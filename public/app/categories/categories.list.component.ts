import {Component, OnInit, OnDestroy, Host, forwardRef, Inject} from '@angular/core';
import {ControlGroup, Control, Validator, Validators, FormBuilder, FORM_DIRECTIVES} from '@angular/common';
import {CanReuse} from '@angular/router-deprecated';
import {isArray} from "rxjs/util/isArray";
import {AppComponent} from '../app.component';
import {EventsManager} from '../lib/events.manager';
import {ConfirmWindow} from '../lib/ui/confirm/confirm.window';
import {UserService} from '../common/services/user.service';
import {CategoryModel} from './models/category.model';
import {CategoriesService} from './services/categories.service';
import {CommonFormValidators} from '../common/validators/common.form.validators';



@Component(
    {
        templateUrl: 'app/categories/categories.list.component.html',
        directives: [
            FORM_DIRECTIVES
        ]
    }
)
export class CategoriesListComponent implements OnInit, OnDestroy, CanReuse {
    list:Array<CategoryModel> = [];
    edit:CategoryModel = null;
    oldName:string;
    public isUser:boolean = false;
    protected subscriptions = [];
    public app:AppComponent;
    public form;


    constructor(@Host() @Inject(forwardRef(() => AppComponent)) app:AppComponent,
                public fb:FormBuilder,
                protected _categoriesService:CategoriesService, protected user:UserService,
                protected events:EventsManager) {
        this.app = app;
        this.createAddCategoryForm();
    }

    ngOnInit() {

        var subscription = this.events.getEmitter(UserService.EVENT_USER_UPDATED).subscribe(event => {
            this.isUser = event.user.user ? true : false;
            this.loadCategories();
        });


        this.subscriptions.push(subscription);
        this.user.sync();
    }


    ngOnDestroy() {
        this.subscriptions.forEach(item => item.unsubscribe());
        this.subscriptions = [];
    }

    hasCategories() {
        return isArray(this.list) && this.list.length;
    }


    loadCategories() {
        this._categoriesService.getAll().subscribe(
            data => {
                let list = [];
                if (data.success && data.data) {
                    list = data.data;
                }
                this.list = list;

            },
            error => console.log(error)
        );

    }

    doAction(action) {

        switch (action) {
            case 'login':
                this.events.getEmitter(AppComponent.EVENT_SHOW_LOGIN_FORM).emit(null);
                break;

            case 'register':
                this.events.getEmitter(AppComponent.EVENT_SHOW_REGISTER_FORM).emit(null);
                break;
        }

        return false;
    }

    routerCanReuse() {
        return false;
    }

    deleteCategory(item:CategoryModel) {



        var subscriber = this.app.getConfirmWindow().show('Удаление категории', 'Удалить категорию "' + item.name + '" ?').subscribe(status => {
            if(status == ConfirmWindow.CONFIRM_STATUS_OK) {
                this._categoriesService.delete(item.id).subscribe(data => {
                    if (data.success) {
                        let c = this.list.length,
                            _list = [];
                        if (c) {

                            for (var i = 0; i < c; i++) {
                                if (this.list[i] === item) {
                                    continue;
                                }
                                _list.push(this.list[i]);
                            }

                            this.list = _list;
                        }
                    }
                });
            }

            subscriber.unsubscribe();
        })
    }

    addCategory() {
        let input = this.form.controls.name;
        if (input.valid) {

            this._categoriesService.save(
                new CategoryModel(0, input.value)
            ).subscribe(data => {
                if (data.success && data.data) {
                    if (!isArray(this.list)) {
                        this.list = [];
                    }
                    this.list.push(data.data);
                    this.createAddCategoryForm();
                }

            });
        }
    }


    startEditName(item:CategoryModel) {
        if (this.edit === null) {
            this.edit = item;
            this.oldName = item.name;
        }
    }

    cancelEdit() {
        this.edit.name = this.oldName;
        this.edit = null;
        this.oldName = '';
    }

    finishEditName(event) {
        var newName = this.edit.name.trim();

        if(event === true || (event.keyCode != undefined && event.keyCode == 13)) {

            if (newName == '' || newName == this.oldName) {
                this.edit.name = this.oldName;
                this.edit = null;
                this.oldName = '';
                return;
            }

            this._categoriesService.save(this.edit).subscribe(data => {
                if (data.success) {
                    this.edit = null;
                    this.oldName = '';
                }
            });
        }
    }

    move(item, direction) {
        this._categoriesService.move(item, direction).subscribe(
            data => {
                if (data.success && data.data) {
                    this.list = data.data;
                }
            },
            error => console.log(error)
        )
    }

    createAddCategoryForm() {
        this.form = this.fb.group({
            'name': ['', CommonFormValidators.notEmptyValidator('Введите название категории')],
        });
    }

}