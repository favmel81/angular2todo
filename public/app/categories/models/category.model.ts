
export class CategoryModel {
    public id:number;
    public name: string;
    public ordered: number;
    public user_id: number;

    public constructor(id: number = 0, name: string, ordered: number = 0, user_id:number=0) {
        this.id = id;
        this.name = name;
        this.ordered = ordered;
        this.user_id = user_id;
    }
}
