import {Component, ViewChild} from '@angular/core';
import { RouteConfig, Router, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import {EventsManager} from './lib/events.manager';
import {HttpService} from './lib/http.service';
import {CategoriesService} from './categories/services/categories.service';
import {TasksService} from './tasks/services/tasks.service';
import {UserService} from './common/services/user.service';
import {CategoriesListComponent} from './categories/categories.list.component';
import {TasksListComponent} from './tasks/tasks.list.component';
import {AccountComponent} from './account/account.component';
import {AccountService} from './account/services/account.service';
import {AuthForm} from './account/forms/auth.form';
import {RegisterForm} from './account/forms/register.form';
import {ConfirmWindow} from './lib/ui/confirm/confirm.window';
import {AlertWindow} from './lib/ui/alert/alert.window';




@RouteConfig(
    [
        {
            path: '/categories',
            name: 'Categories',
            component: CategoriesListComponent,
            useAsDefault: true
        },
        {
            path: '/',
            name: 'Home',
            component: TasksListComponent
        },
        {
            path: '/account',
            name: 'Account',
            component: AccountComponent
        }
    ]
)
@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    directives: [
        ROUTER_DIRECTIVES,
        AuthForm,
        RegisterForm,
        ConfirmWindow,
        AlertWindow
    ],
    providers: [
        CategoriesService,
        TasksService,
        AccountService
    ]
})
export class AppComponent {

    static EVENT_SHOW_LOGIN_FORM = 'event_show_login_form';
    static EVENT_SHOW_REGISTER_FORM = 'event_show_register_form';

    @ViewChild(AuthForm) authForm: AuthForm;
    @ViewChild(RegisterForm) registerForm: RegisterForm;
    @ViewChild(ConfirmWindow) confirmWindow: ConfirmWindow;
    @ViewChild(AlertWindow) alertWindow: AlertWindow;



    public isUser: boolean = false;

    constructor(protected user: UserService, protected accountService: AccountService, protected router: Router, protected events: EventsManager) {

        this.events.getEmitter(HttpService.EVENT_AFTER_EXTRACT_DATA).subscribe(data => {
               if(data.alert) {
                   this.getAlertWindow().show(data.alert);
               } else if(data.auth) {
                   this.getAlertWindow().show({
                       message: 'Ошибка доступа! Страница будет перегружена!',
                       timeout: 10,
                       redirect: '/'
                   });
               }

        });

        this.events.getEmitter(UserService.EVENT_USER_UPDATED).subscribe(event => {
            this.isUser = event.user.user ? true: false
        });

        this.events.getEmitter(AppComponent.EVENT_SHOW_LOGIN_FORM).subscribe(() => {
            this.showAuthForm();
        });

        this.events.getEmitter(AppComponent.EVENT_SHOW_REGISTER_FORM).subscribe(() => {
            this.showRegisterForm();
        });
    }

    getUserName() {
        if(this.user.user) {
            return this.user.user.login;
        }
    }


    showAuthForm() {
        this.authForm.show();
        return false;
    }

    showRegisterForm() {
        this.registerForm.show();
        return false;
    }

    logout() {

        var subscriber = this.confirmWindow.show('Выход', 'Уверены, что хотите выйти?').subscribe(status => {
            if(status == ConfirmWindow.CONFIRM_STATUS_OK) {

                this.accountService.logout().subscribe(() => {
                    this.user.sync();
                });

            }
            subscriber.unsubscribe();
        });

        return false;
    }


    getConfirmWindow() {
        return this.confirmWindow;
    }

    getAlertWindow() {
        return this.alertWindow;
    }
}
